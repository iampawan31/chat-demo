<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Events\MessagePosted;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function getMessages()
    {
        return Message::with('user')->get();
    }

    public function postMessages(Request $request)
    {
        $user = auth()->user();
        $message = $user->messages()->create([
            'message' => $request->get('message')
        ]);

        broadcast(new MessagePosted($message, $user))->toOthers();
        return ['status' => 'OK'];
    }
}
