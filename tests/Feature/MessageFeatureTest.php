<?php

namespace Tests\Feature;

use App\User;
use App\Events\MessagePosted;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class MessageFeatureTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    /** @test */
    public function user_can_send_a_message()
    {
        $user = factory(User::class)->create();

        Event::fake();
        $response = $this->actingAs($user)
        ->post('messages', [
            'message' => 'Hello There'
            ]);

        Event::assertDispatched(MessagePosted::class);

    }
}
