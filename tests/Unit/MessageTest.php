<?php

namespace Tests\Unit;

use App\Message;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MessageTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    /** @test */
    public function user_can_create_a_message()
    {
        $message = factory(Message::class)->create([
            'user_id' => function () {
                return factory(User::class)->create()->id;
            },
            ]);

        $this->assertEquals($message->id, Message::first()->id);
    }
}
